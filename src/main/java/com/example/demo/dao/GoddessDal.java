package com.example.demo.dao;

import com.example.demo.controller.Goddess;
import com.example.demo.utils.DBUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
//数据库操作 查询
public class GoddessDal {
    //添加数据，传入Goddess对象
    public void addGoddes(Goddess goddess) throws SQLException {
        //创建数\获得数据库的连接据库连接
        Connection conn = DBUtil.getConnection();
        //编写sql语句
        String sql = "INSERT INTO jnshu_goddess (user_name,sex,age,birthday,email,mobile,create_user,create_date,update_user,update_date,isdel)" +
                "VALUE(?,?,?,?,?,?,?,current_date(),?,current_date(),?)";
//        预编译sql语句
        PreparedStatement preparedStatement = conn.prepareStatement(sql);
        preparedStatement.setString(1,goddess.getUser_name());
        preparedStatement.setInt(2,goddess.getSex());
        preparedStatement.setInt(3,goddess.getAge());
        preparedStatement.setDate(4,new Date(goddess.getBirthday().getTime()));
        preparedStatement.setString(5,goddess.getMobile());
        preparedStatement.setString(6,goddess.getMobile());
        preparedStatement.setString(7,goddess.getCreate_user());
        preparedStatement.setString(8,goddess.getUpdate_user());
        preparedStatement.setInt(9,goddess.getIsdel());
//        输出preparedstatement对象内容
        System.out.println(preparedStatement.toString());
        //执行sql语句
        preparedStatement.execute();
    }
    public List<Goddess> querys() throws SQLException {
        Connection conn = DBUtil.getConnection();
        Statement statement = conn.createStatement();
        ResultSet resultSet = statement.executeQuery("SELECT user_name,age FROM jnshu_goddess");

        List<Goddess> goddessess = new ArrayList<Goddess>();
        Goddess goddess = null;
        while (resultSet.next()){
            goddess = new Goddess();
            goddess.setUser_name(resultSet.getString("user_name"));
            goddess.setAge(resultSet.getInt("age"));
            goddessess.add(goddess);
        }
        return goddessess;
    }
}
