package com.example.demo.action;

import com.example.demo.controller.Goddess;
import com.example.demo.dao.GoddessDal;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
//数据操作
import java.util.Date;
@RestController
public class GoddessAction {
    @RequestMapping("/main")
    public static void main(String[] args) throws Exception {
        //创建Goddess对象
        Goddess gl = new Goddess();
        //设置对象的值
        gl.setUser_name("小霞");
        gl.setSex(1);
        gl.setAge(23);
        gl.setBirthday(new Date());
        gl.setEmail("1947324563@qq.com");
        gl.setMobile("13164146690");
        gl.setCreate_user("ADMIN");
        gl.setUpdate_user("ADMIN");
        gl.setIsdel(1);
        GoddessDal g = new GoddessDal();
        for (int j = 0; j < 100;j++){
            g.addGoddes(gl);
            System.out.println(j);
        }
        System.out.println("插入完成");
    }
}
