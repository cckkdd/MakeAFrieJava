package com.example.demo.utils;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DBUtil {
    //驱动包和数据库url
    private static String url = null;
    private static String driverClass = null;
    //数据库用户名和密码
    private static String userName = null;
    private static String passWord = null;
    static {
        //读取db.properties文件
        Properties properties = new Properties();
        //使用类路劲的读取方法
        InputStream resourceAsStream = DBUtil.class.getResourceAsStream("/config.application.properties");
        //加载文件
        try{
            properties.load(resourceAsStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    //数据库连接
    public static Connection getConnection() throws SQLException {
        Connection conn = DriverManager.getConnection(url,userName,passWord);
        System.out.println(conn);
        return conn;
    }
}
